const NewsletterCard = () => {
  return (
    <div class="bg-gray-200 rounded-md p-5">
      <p class="font-semibold text-2xl text-gray-700 mb-2">Newsletter</p>
      <p class="text-gray-500 text-[15px] mb-4">
        Thank you for your interest in joining my personal blog community! By subscribing, you'll receive exclusive updates directly in           your inbox, ensuring you never miss out on my latest articles and insights. Join me on this journey of personal growth, knowledge sharing
    	</p>
      <input 
	      class="w-full bg-slate-50 border border-slate-200 h-10 rounded-md focus:outline-none focus:bg-white px-4 placeholder-slate-400 font-medium mb-4 lg:mb-0" 
        placeholder="Your Email"
	    />
    </div>
  )
}

export default NewsletterCard;
