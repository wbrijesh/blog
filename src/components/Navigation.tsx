const Navigation = () => {
  const NavLinksInternal = [
    {
      text: "About Me",
      url: "/about-me"
    },
    {
      text: "Work",
      url: "/work"
    },
    {
      text: "Blog",
      url: "/blog"
    }
  ];

  const NavLinksExternal = [
    {
      text: "Github",
      url: "https://github.com/wbrijesh"
    },
    {
      text: "Goodreads",
      url: "https://goodreads.com"
    }
  ]

  return (
    <div class="flex flex-col lg:gap-0 gap-3 mb-6">
      <p class="text-orange-600 font-medium text-md lg:mb-3">Brijesh's blog</p>

      <div class="flex lg:flex-col lg:gap-1 gap-3 lg:mb-3">
          {NavLinksInternal.map((link: any) => 
              <a 
                class="text-md font-medium text-gray-500 hover:text-gray-600 hover:underline decoration-dashed" 
                href={link.url}
              >
                {link.text}
              </a>
          )}
      </div>

      <div class="flex lg:flex-col lg:gap-1 gap-3 lg:mb-3">
          <p class="uppercase text-gray-400 font-medium text-sm mt-3 mb-2 hidden lg:block">External links</p>
          {NavLinksExternal.map((link: any) => 
              <a class="flex items-center gap-1.5 text-md font-medium text-gray-500 hover:underline decoration dashed haver:text-gray-600" href={link.url}>
                  <span>
                      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-move-up-right"><path d="M13 5H19V11"/><path d="M19 5L5 19"/></svg>
                  </span>
                  <span>
                      {link.text}
                  </span>
              </a>
              )}
      </div>
    </div>
  )
}

export default Navigation;
