import Navigation from './Navigation'

const Layout = ({children, title}) => {
  return (
    <html lang="en">
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width" />
      <meta name="generator" content={Astro.generator} />
      <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
      <title>{title}</title>
    </head>
    <body>
      <div class="relative p-4">
        <nav class="lg:fixed lg:h-full lg:left-4 max-w-2xl mx-auto">
          <Navigation />
        </nav>
        <main class="max-w-[640px] mx-auto my-6">
          {children}
        </main>
      </div>
    </body>
    </html>

  )
}

export default Layout;
