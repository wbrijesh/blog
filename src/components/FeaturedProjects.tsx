import { Show } from 'solid-js';

const ProjectType = {
  WEBSITE: "Website",
  APP: "App",
  STARTUP: "Startup",
  SAAS: "SaaS",
  CLI: "CLI",
  ENVIRONMENT: "Environment",
};

const featuredProjects = [
  {
    name: "Watchman",
    description: "Lightweight tool for system monitoring",
    category: ProjectType.APP,
  },
  {
    name: "VC",
    description: "CLI tool for client side git mirror management",
    category: ProjectType.CLI,
  },
  {
    name: "Core Analytics",
    description: "Minimal and privacy friendly analytics",
    category: ProjectType.SAAS,
  },
  {
    name: "Homelab",
    description: "Setup for my simple homeserver",
    category: ProjectType.ENVIRONMENT,
  }
];

const FeaturedProjects = () => {
  return (
    <div class="space-y-1">
      <For each={featuredProjects}>{(project, index) =>
        <div class="flex flex-col gap-1">
          <Show when={index() == 0}>
            <div
              class="flex items-center justify-between bg-gray-200 p-3 hover:bg-gray-300 transition rounded-t-xl rounded-b"
            >
              <div class="space-y-0.5">
                <p class="font-medium text-sm">{project.name}</p>
                <p class="text-sm text-slate-700">{project.description}</p>
              </div>
              <p class="monospace text-sm uppercase text-slate-600">{project.category}</p>
            </div>
          </Show>

          <Show when={index() !== featuredProjects.length - 1 && index() !== 0}>
            <div
              class="flex items-center justify-between bg-gray-200 p-3 hover:bg-gray-300 transition rounded"
            >
              <div class="space-y-0.5">
                <p class="font-medium text-sm">{project.name}</p>
                <p class="text-sm text-slate-700">{project.description}</p>
              </div>
              <p class="monospace text-sm uppercase text-slate-600">{project.category}</p>
            </div>
          </Show>

          <Show when={index() == featuredProjects.length - 1}>
            <div
              class="flex items-center justify-between bg-gray-200 p-3 hover:bg-gray-300 transition rounded-b-xl rounded-t"
            >
              <div class="space-y-0.5">
                <p class="font-medium text-sm">{project.name}</p>
                <p class="text-sm text-slate-700">{project.description}</p>
              </div>
              <p class="monospace text-sm uppercase text-slate-600">{project.category}</p>
            </div>
          </Show>
        </div>
      }</For>
    </div>
  );
}

export default FeaturedProjects;
