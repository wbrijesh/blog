const Section = ({title, children}: {title: string}) => {
  return (
    <section class="space-y-5 mb-14">
      <h2 class="font-medium text-xl">{title}</h2>
      {children}
    </section>
  )
}

export default Section;
